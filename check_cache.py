#!/usr/bin/env python3

import sys

try:
  import argparse
except ImportError:
  print('argparse package not found, please install')
  sys.exit(3)

try:
  import requests
except ImportError:
  print('requests package not found, please install')
  sys.exit(3)

try:
  from nagplug import Threshold, Plugin, OK, WARNING, CRITICAL, UNKNOWN
except ImportError:
  print('nagplug package not found, please install')
  sys.exit(3)

if __name__ == '__main__':

  # check_cache.py -k sensors/rtl_433/1/temperature_C -w 30 -c 40 -r "Office" 
  parser = argparse.ArgumentParser(prog='check_cache.py')
  parser.add_argument('-k', '--key',  help='url to get the data eg. http://server/api?q=office/temperature', required=True)
  parser.add_argument('-w', '--warn', help='warning threshold value')
  parser.add_argument('-c', '--crit', help='critical threshold value')
  parser.add_argument('-p', '--pre', help='output text prefix', default="")
  parser.add_argument('-s', '--suf', help='output text suffix', default="")
  parser.add_argument('-n', '--name', help='check name', default="")

  args = parser.parse_args()

plugin = Plugin(args.name)

response = requests.get(args.key)
if response.status_code != 200:
  print("UNK - No data found.")
  sys.exit(3)

value = int(float(response.text)) if int(float(response.text)) == float(response.text) else float(response.text)
crit = Threshold(args.crit) if args.crit is not None else None
warn = Threshold(args.warn) if args.warn is not None else None

if "temperature" in args.pre.lower() or "temperature" in args.suf.lower():
  plugin.add_perfdata('temperature', value, warning=warn, critical=crit)

if "humidity" in args.pre.lower() or "humidity" in args.suf.lower():
  plugin.add_perfdata('humidity', value, uom='%', minimum=0, maximum=100, warning=warn, critical=crit)

value_status = plugin.check_threshold(value, warning=warn, critical=crit)
plugin.add_result(value_status, args.pre + str(value) + args.suf)
plugin.finish()
print(plugin.get_message())
sys.exit(plugin.get_code())
